QEMU-images
===========

QEMU-images is a simple tool to download, update and repack QEMU images.

Features
========

QEMU-image is able to:

* locate and fetch the latest cloud image from Debian
* install some additional packages
* repack and compress the resulting image
* upload it to an artifactorial server
* test it using LAVA


Requirements
============

QUEM-images requires some system binaries:

* guestfish
* qemu-img
* virt-customize
* zstd

On a Debian system, you can install the system dependencies using:

```shell
apt-get install libguestfs-tools qemu-utils zstd
```

QEM-images also requires some python libraries, listed in `requirements.txt`:

```shell
python3 -m pip install -r requirements.txt
```


Usage
=====

In order to use QEMU-image, you should just run:

```shell
python3 run.py
```

QEMU-image will locate, download and update the Debian cloud images and store
them into `output/` sub-directory.

Uploading to Artifactorial
--------------------------

QEMU-image can also push to a given Artifactorial server if the environment variables are defined:

```shell
export ARTIFACTORIAL_URL="https://archive.validation.linaro.org/artifacts/team/lava/"
export ARTIFACTORIAL_TOKEN="<artifactorial-url>"
```

Testing with LAVA
-----------------

QEMU-image can also test the resulting image with a provided LAVA instance. You
should define the following environment variables:

```shell
export LAVA_URL="https://staging.validation.linaro.org"
export LAVA_TOKEN="<lava-token>"
```
