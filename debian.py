# -*- coding: utf-8 -*-
# vim: set ts=4
#
# Copyright 2020-present Linaro Limited
#
# Author: Rémi Duraffort <remi.duraffort@linaro.org>
#
# SPDX-License-Identifier: MIT

import argparse
import contextlib
from dataclasses import dataclass
from pathlib import Path
import shlex
import subprocess
import sys

from bs4 import BeautifulSoup
import requests


#############
# Constants #
#############
@dataclass
class Colors:
    blue: str = "\x1b[1;34;40m"
    purple: str = "\x1b[1;35;40m"
    red: str = "\x1b[1;31;40m"
    white: str = "\x1b[1;37;40m"
    yellow: str = "\x1b[1;33;40m"
    reset: str = "\x1b[0m"


if sys.stdout.isatty():
    COLORS = Colors()
else:
    COLORS = Colors("", "", "", "", "", "")

TIMEOUT = 10
URL = "https://cloud.debian.org/images/cloud/"


#########
# Globals
#########
# Create a requests session and use it for every request.
SESSION = requests.Session()


###########
# Helpers #
###########
def find_last_directory(url: str) -> str:
    ret = SESSION.get(url, timeout=TIMEOUT)
    ret.raise_for_status()
    html = BeautifulSoup(ret.content, features="lxml")

    evens = html.find_all("tr", class_="even")[1:]
    odds = html.find_all("tr", class_="odd")

    trs = evens + odds
    urls = sorted([tr.contents[0].contents[0]["href"] for tr in trs])
    return url + urls[-1]


def find_last_image(url: str) -> str:
    ret = SESSION.get(url, timeout=TIMEOUT)
    ret.raise_for_status()
    html = BeautifulSoup(ret.content, features="lxml")

    for a in html.find_all("a"):
        if "nocloud" in a["href"] and a["href"].endswith("qcow2"):
            return url + a["href"]
    raise NotImplementedError("Unable to find the image")


def download_file(url: str, dest: Path) -> None:
    with dest.open("wb") as fout:
        with SESSION.get(url, stream=True) as ret:
            ret.raise_for_status()
            for data in ret.iter_content(32768):
                fout.write(data)


def run(cmd: str) -> None:
    print(f"{COLORS.blue}$ {Colors.white}{cmd}{Colors.reset}")

    ret = subprocess.call(shlex.split(cmd))
    if ret != 0:
        raise Exception("Unable to run '%s', returned %d" % (cmd, ret))


##########
# Setups #
##########
def cleanup(filename: Path, filename_raw: Path, filename_zst: Path) -> None:
    with contextlib.suppress(FileNotFoundError):
        filename.unlink()
    with contextlib.suppress(FileNotFoundError):
        filename_raw.unlink()
    with contextlib.suppress(FileNotFoundError):
        filename_zst.unlink()


def setup_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(description="qemu-image")
    parser.add_argument("codename", type=str, help="Debian codename")
    parser.add_argument(
        "output", type=Path, nargs="?", default="output", help="Output directory"
    )

    return parser


##############
# Entrypoint #
##############
def main() -> int:
    # Parse command line
    options = setup_parser().parse_args()

    options.output.mkdir(mode=0o755, exist_ok=True, parents=True)
    filename = options.output / f"debian-{options.codename}.qcow2"
    filename_raw = filename.with_suffix(".img")
    filename_zst = filename.with_suffix(".qcow2.zst")
    cleanup(filename, filename_raw, filename_zst)

    try:
        print(
            f"Building for {COLORS.red}Debian/{options.codename}{Colors.reset} in {COLORS.red}{options.output}{COLORS.reset}"
        )

        url = f"{URL}{options.codename}/daily/"
        print(f"> Parsing {COLORS.purple}{url}{Colors.reset}")
        directory = find_last_directory(url)

        print(f"> Parsing {COLORS.purple}{directory}{Colors.reset}")
        image = find_last_image(directory)

        print(
            f"> Downloading {COLORS.yellow}{image}{Colors.reset} to {Colors.yellow}{filename}{Colors.reset}"
        )
        download_file(image, filename)

        # TODO: install more packages?
        print(f"> Install {COLORS.yellow}netcat{Colors.reset}")
        run(f"virt-customize -a {filename} --install netcat")

        print("> Convert to raw")
        run(f"qemu-img convert -f qcow2 -O raw {filename} {filename_raw}")
        filename.unlink()
        print("> Run zerofree")
        run(f"guestfish -a {filename_raw} run : zerofree /dev/sda1")
        print("> Convert back to qcow2")
        run(f"qemu-img convert -c -f raw -O qcow2 {filename_raw} {filename}")
        filename_raw.unlink()
        print("> Compress using zstd")
        run(f"zstd {filename}")
        filename.unlink()
    except requests.RequestException as exc:
        print(f"\n{COLORS.red}Unable to download{COLORS.reset}: {str(exc)}")
        cleanup(filename, filename_raw, filename_zst)
        return 1
    except KeyboardInterrupt:
        print(f"\n{COLORS.red}Received Ctrl+c, leaving{COLORS.reset}")
        cleanup(filename, filename_raw, filename_zst)
        return 1

    return 0


if __name__ == "__main__":
    sys.exit(main())
